defmodule Autobeep.Plugs.RequireAuth do
    import Plug.Conn
    import Phoenix.Controller
  
    alias Autobeep.Router.Helpers
  
    def init(_params) do
    end
  
    def call(conn, _params) do
      IO.inspect conn
      if conn.assigns[:user] do
        conn
      else
        conn
        |> put_flash(:error, "Debe iniciar sesión.")
        |> redirect(to: Helpers.page_path(conn, :index))
        |> halt()
      end
    end
  
  end