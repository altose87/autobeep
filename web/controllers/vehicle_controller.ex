defmodule Autobeep.VehicleController do
  use Autobeep.Web, :controller

  alias Autobeep.Vehicle
  
  plug Autobeep.Plugs.RequireAuth when action in [:index, :new, :create, :show, :edit, :update, :delete]
  plug :check_vehicle_owner when action in [:edit, :update, :delete]

  def index(conn, _params) do
    vehicles = Repo.all(Vehicle)
    render(conn, "index.html", vehicles: vehicles)
  end

  def new(conn, _params) do
    changeset = Vehicle.changeset(%Vehicle{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"vehicle" => vehicle_params}) do
    # This is the way to make an association
    IO.inspect conn.assigns.user
    changeset = conn.assigns.user
    |> build_assoc(:vehicles)
    |> Vehicle.changeset(vehicle_params)

    case Repo.insert(changeset) do
      {:ok, _vehicle} ->
        conn
        |> put_flash(:info, "Se creó correctamente el vehículo.")
        |> redirect(to: vehicle_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    vehicle = Repo.get!(Vehicle, id)
    render(conn, "show.html", vehicle: vehicle)
  end

  def edit(conn, %{"id" => id}) do
    vehicle = Repo.get!(Vehicle, id)
    changeset = Vehicle.changeset(vehicle)
    render(conn, "edit.html", vehicle: vehicle, changeset: changeset)
  end

  def update(conn, %{"id" => id, "vehicle" => vehicle_params}) do
    vehicle = Repo.get!(Vehicle, id)
    changeset = Vehicle.changeset(vehicle, vehicle_params)

    case Repo.update(changeset) do
      {:ok, vehicle} ->
        conn
        |> put_flash(:info, "Se actualizó correctamente el vehículo.")
        |> redirect(to: vehicle_path(conn, :show, vehicle))
      {:error, changeset} ->
        render(conn, "edit.html", vehicle: vehicle, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    vehicle = Repo.get!(Vehicle, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(vehicle)

    conn
    |> put_flash(:info, "Se eliminó correctamente el vehículo.")
    |> redirect(to: vehicle_path(conn, :index))
  end
  
  def check_vehicle_owner(conn, _params) do
    %{params: %{"id" => vehicle_id}} = conn
    if Repo.get(Vehicle, vehicle_id).user_id == conn.assigns.user.id do
      conn
    else
      conn
      |> put_flash(:error, "No eres el propietario del vehículo.")
      |> redirect(to: vehicle_path(conn, :index))
      |> halt()
    end
  end
end
