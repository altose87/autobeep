defmodule Autobeep.Router do
  use Autobeep.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Autobeep.Plugs.SetUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Autobeep do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
    resources "/vehicle", VehicleController
  end
  
  scope "/auth", Autobeep do
    pipe_through :browser

    get "/signout", AuthController, :signout

    # Request phase paths
    get "/:provider", AuthController, :request
    # Callback phase paths
    get "/:provider/callback", AuthController, :callback
  end

  # Other scopes may use custom stacks.
  # scope "/api", Autobeep do
  #   pipe_through :api
  # end
end
