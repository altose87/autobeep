defmodule Autobeep.Vehicle do
  use Autobeep.Web, :model

  schema "vehicles" do
    field :brand, :string
    field :reference, :string
    field :model, :string
    field :plate, :string
    field :mileage, :string
    field :type, :string
    belongs_to :user, Autobeep.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:brand, :reference, :model, :plate, :mileage, :type])
    |> validate_required([:brand, :plate])
  end
end
