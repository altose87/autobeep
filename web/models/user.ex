defmodule Autobeep.User do
  use Autobeep.Web, :model

  schema "users" do
    field :email, :string
    field :provider, :string
    field :token, :string
    field :avatar_url, :string
    field :uuid_token, :string
    field :first_name, :string
    field :last_name, :string
    field :birth_date, :date
    field :phonenumber, :string
    has_many :vehicles, Autobeep.Vehicle

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :provider, :token, :uuid_token, :avatar_url, :first_name, :last_name, :birth_date, :phonenumber])
    |> validate_required([:email, :provider, :token])
  end
end