defmodule Autobeep.VehicleControllerTest do
  use Autobeep.ConnCase

  alias Autobeep.Vehicle
  @valid_attrs %{brand: "some content", mileage: "some content", model: "some content", plate: "some content", reference: "some content", type: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, vehicle_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing vehicles"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, vehicle_path(conn, :new)
    assert html_response(conn, 200) =~ "New vehicle"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, vehicle_path(conn, :create), vehicle: @valid_attrs
    assert redirected_to(conn) == vehicle_path(conn, :index)
    assert Repo.get_by(Vehicle, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, vehicle_path(conn, :create), vehicle: @invalid_attrs
    assert html_response(conn, 200) =~ "New vehicle"
  end

  test "shows chosen resource", %{conn: conn} do
    vehicle = Repo.insert! %Vehicle{}
    conn = get conn, vehicle_path(conn, :show, vehicle)
    assert html_response(conn, 200) =~ "Show vehicle"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, vehicle_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    vehicle = Repo.insert! %Vehicle{}
    conn = get conn, vehicle_path(conn, :edit, vehicle)
    assert html_response(conn, 200) =~ "Edit vehicle"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    vehicle = Repo.insert! %Vehicle{}
    conn = put conn, vehicle_path(conn, :update, vehicle), vehicle: @valid_attrs
    assert redirected_to(conn) == vehicle_path(conn, :show, vehicle)
    assert Repo.get_by(Vehicle, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    vehicle = Repo.insert! %Vehicle{}
    conn = put conn, vehicle_path(conn, :update, vehicle), vehicle: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit vehicle"
  end

  test "deletes chosen resource", %{conn: conn} do
    vehicle = Repo.insert! %Vehicle{}
    conn = delete conn, vehicle_path(conn, :delete, vehicle)
    assert redirected_to(conn) == vehicle_path(conn, :index)
    refute Repo.get(Vehicle, vehicle.id)
  end
end
