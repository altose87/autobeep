defmodule Autobeep.VehicleTest do
  use Autobeep.ModelCase

  alias Autobeep.Vehicle

  @valid_attrs %{brand: "some content", mileage: "some content", model: "some content", plate: "some content", reference: "some content", type: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Vehicle.changeset(%Vehicle{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Vehicle.changeset(%Vehicle{}, @invalid_attrs)
    refute changeset.valid?
  end
end
