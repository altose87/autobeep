# Welcome to Autobeep

To start autobeep you need to:

  * Clone the repository in your local machine
  * Install the project dependencies by running `mix deps.get`
  * Create and migrate your database by running `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can open your browser and navigate to [`localhost:4000`](http://localhost:4000)

# What is Autobeep?

Autobeep is a startup project born in Apps.co in 2016 by a group of friends.

Autobeep is in charge of keeping the resume of the vehicles, motorcycles or cars, and keeping the owner informed of the needs of these by alerts that let him know when the obligations of his vehicle must be carried out.

In addition, it offers the user a complete summary of all the vehicle payments, these payments are maintenances, obligations, refueling, etc.

# Tools

  * JS and CSS with [`Materialize`](http://materializecss.com/)
  * OAuth with [`Üeberauth`](https://github.com/ueberauth/ueberauth)

# Notes

  * Facebook Login: to get the login with facebook working for localhost its necessary to add the localhost URL in the `Valid OAuth redirect URIs` to do that, navigate to [`Facebook App`](https://developers.facebook.com/apps/136010227105508/fb-login/)