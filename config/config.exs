# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :autobeep,
  ecto_repos: [Autobeep.Repo]

# Configures the endpoint
config :autobeep, Autobeep.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZSl3f8oxB3oAU0Z3ASTwNDJuMXfjZ5MHNHO+rNxtmid/aGo18hnuO/mKf6ixJ9VK",
  render_errors: [view: Autobeep.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Autobeep.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :ueberauth, Ueberauth,
providers: [
  facebook: {Ueberauth.Strategy.Facebook, [
    default_scope: "email,public_profile,user_friends",
    profile_fields: "name,email,first_name,last_name",
    display: "popup"
  ]},
  google: {Ueberauth.Strategy.Google, [default_scope: "email"]}
]

config :ueberauth, Ueberauth.Strategy.Facebook.OAuth,
  client_id: "136010227105508",
  client_secret: "8c56c89c110db3e1a6eed0d6bd4431c7"
  # client_id: System.get_env("FACEBOOK_CLIENT_ID"),
  # client_secret: System.get_env("FACEBOOK_CLIENT_SECRET")

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  client_id: "376496971067-h2qpvov5itd571rbnh86nr5nm5db78e3.apps.googleusercontent.com",
  client_secret: "ygwwylfI31YJvzFvfX3A4uLI"
  # client_id: System.get_env("GOOGLE_CLIENT_ID"),
  # client_secret: System.get_env("GOOGLE_CLIENT_SECRET")