defmodule Autobeep.Repo.Migrations.CreateVehicle do
  use Ecto.Migration

  def change do
    create table(:vehicles) do
      add :brand, :string
      add :reference, :string
      add :model, :string
      add :plate, :string
      add :mileage, :string
      add :type, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:vehicles, [:user_id])

  end
end
