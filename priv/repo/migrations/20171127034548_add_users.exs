defmodule Autobeep.Repo.Migrations.AddUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :provider, :string
      add :token, :string
      add :avatar_url, :string
      add :uuid_token, :string
      add :first_name, :string
      add :last_name, :string
      add :birth_date, :date
      add :phonenumber, :string

      timestamps()
    end
  end
end
